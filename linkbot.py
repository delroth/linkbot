#! /usr/bin/env python3
# -*- encoding: utf-8
#
# linkbot.py
# Main file of the #lse link bot.

import chardet
import io
import lxml.html
import pypeul
import pyPdf
import queue
import re
import sys
import threading
import traceback
import urllib.error
import urllib.request
import yaml

def load_config():
    if len(sys.argv) >= 2:
        cfgfile = sys.argv[1]
    else:
        cfgfile = "config.yml"

    with open(cfgfile) as fp:
        return yaml.load(fp)

class BackendThread(threading.Thread):
    def __init__(self, cfg):
        super(BackendThread, self).__init__()
        self._cfg = cfg
        self._queue = queue.Queue()

        self.init_backends()

    def init_backends(self):
        self._backends = []
        for backend_names in self._cfg['backends']:
            mod = __import__('backends.%s' % backend_names,
                             fromlist=['Backend'])
            self._backends.append(mod.Backend(self._cfg))

    def process(self, url, title, tags):
        self._queue.put((url, title, tags))

    def title_from_url(self, url):
        try:
            fp = urllib.request.urlopen(url)
        except urllib.error.URLError:
            return None
        except urllib.error.HTTPError:
            return None

        headers = fp.info()
        contents = fp.read(30 * 2**20) # Max downloaded size is 30MB

        content_type = headers['Content-Type']
        if 'html' in content_type: # xhtml, html, etc.
            return self.title_from_html(contents)
        elif 'application/pdf' in content_type:
            return self.title_from_pdf(contents)
        else:
            return None

    def title_from_html(self, contents):
        encoding = chardet.detect(contents)
        if encoding is not None:
            encoding = encoding["encoding"]
        else:
            encoding = "utf-8" # Pray.

        contents = contents.decode(encoding)

        # Fix a LXML bug.
        contents = re.sub(r'^\s*<\?xml\s+.*?\?>', '', contents)

        doc = lxml.html.fromstring(contents)
        title = doc.cssselect('title')
        if not title:
            return None
        return title[0].text

    def title_from_pdf(self, contents):
        sio = io.BytesIO(contents)
        reader = pyPdf.PdfFileReader(sio)
        title = reader.getDocumentInfo().title

        # Sanity check, some PDF have garbage instead of a title
        if title and len(title) < 20:
            return None
        return title

    def run(self):
        while True:
            url, title, tags = self._queue.get()
            try:
                if title is None:
                    title = self.title_from_url(url)

                for backend in self._backends:
                    backend.handle_link(url, title, tags)
            except Exception:
                print("Something bad happened :(", file=sys.stderr)
                traceback.print_exc(file=sys.stderr)
            self._queue.task_done()

class LinkBot(pypeul.IRC):
    def __init__(self, cfg, backend):
        super(LinkBot, self).__init__()
        self._cfg = cfg
        self._backend = backend
        self._cached_parsing_re = None

        self.connect(self._cfg['irc']['server'],
                     self._cfg['irc']['port'],
                     self._cfg['irc']['ssl'])
        self.ident(self._cfg['irc']['nick'])

    def on_ready(self):
        self.join(self._cfg['irc']['channel'])

    def on_server_privmsg(self, umask, target, msg):
        if target == self._cfg['irc']['nick']:
            try:
                password, rest = msg.split(None, 1)
            except ValueError:
                return

            if not self.is_master(umask.nick, password):
                self.message(umask.nick, "Permission denied.")
                return

            self.handle_master_command(umask, rest)
        elif target == self._cfg['irc']['channel']:
            if (umask.user.modes_in(target) & set('ohv')) == set():
                return

            try:
                url, title, tags = self.parse_link(msg)
                self._backend.process(url, title, tags)
            except ValueError:
                return

    def is_master(self, nick, password):
        for creds in self._cfg['masters']:
            if nick == creds["username"] and password == creds["password"]:
                return True
        return False

    def handle_master_command(self, umask, cmd):
        if cmd == "quit":
            self.quit("Requested by bot master")
        else:
            self.message(umask.nick, "Unknown command.")

    def parse_link(self, msg):
        if self._cached_parsing_re is None:
            self._cached_parsing_re = re.compile(
                r'^(http[s]?://\S*?)\s+(?:\|(.+?)\|\s+)?#((?:\w+?(?:,\s*|$))+)$'
            )

        mobj = self._cached_parsing_re.match(msg.strip())
        if mobj is None:
            raise ValueError("invalid link format")
        url, title, tags = mobj.groups()
        tags = tags.split(',')
        tags = [t.strip() for t in tags if t.strip()]
        return url, title, tags

if __name__ == '__main__':
    try:
        cfg = load_config()
    except IOError:
        print("Could not open the linkbot configuration file", file=sys.stderr)
        print("usage: ./linkbot.py [config.yml]", file=sys.stderr)
        sys.exit(1)
    except yaml.YAMLError as exc:
        print("Syntax error in the configuration file:", file=sys.stderr)
        print(exc, file=sys.stderr)
        sys.exit(2)

    backend = BackendThread(cfg)
    backend.daemon = True
    backend.start()

    bot = LinkBot(cfg, backend)
    bot.run()
