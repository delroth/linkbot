#! /usr/bin/env python3
# -*- encoding: utf-8

import urllib.parse
import urllib.request

class Backend:
    def __init__(self, cfg):
        self._cfg = cfg

        manager = urllib.request.HTTPPasswordMgrWithDefaultRealm()
        manager.add_password(
            realm=None,
            uri="https://api.del.icio.us/v1/",
            user=self._cfg["delicious"]["username"],
            passwd=self._cfg["delicious"]["password"]
        )
        handler = urllib.request.HTTPBasicAuthHandler(manager)
        self._opener = urllib.request.build_opener(handler)

    def handle_link(self, url, title, tags):
        req_url = 'https://api.del.icio.us/v1/posts/add?'
        req_url += 'url=' + urllib.parse.quote_plus(url)

        if title is None:
            title = url
        req_url += '&description=' + urllib.parse.quote_plus(title)

        req_url += '&tags=' + ','.join(urllib.parse.quote_plus(t)
                                       for t in tags)
        self._opener.open(req_url).read()
