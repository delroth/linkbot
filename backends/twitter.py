#! /usr/bin/env python3
# -*- encoding: utf-8

from twitter import Twitter, OAuth

class Backend:
    def __init__(self, cfg):
        self._cfg = cfg
        auth = OAuth(
            token=self._cfg['twitter']['token'],
            token_secret=self._cfg['twitter']['token_secret'],
            consumer_key=self._cfg['twitter']['consumer_key'],
            consumer_secret=self._cfg['twitter']['consumer_secret']
        )
        self._tw = Twitter(auth=auth)

    def handle_link(self, url, title, tags):
        status_len = 20 + sum(2 + len(t) for t in tags)
        max_title_len = 140 - status_len - 1
        if title is not None and len(title) > max_title_len:
            title = title[:max_title_len - 1] + "…"
        
        tags = ' '.join('#' + t for t in tags)
        if title is not None:
            text = "%s %s %s" % (title, url, tags)
        else:
            text = "%s %s" % (url, tags)
        self._tw.statuses.update(status=text)
